#include "tal.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include <wiring_private.h>
#else
#include <wiring.h>
#include <HardwareSerial.h>
#endif

#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

/*
 * NOTE!
 * As in v1.6.2 spec One-Net doesn't support 2.4GHz GFSK modules.
 *
 */

#define PIPE_ADDRESS  (0xF0F0F0F0E1LL)




static RF24 radio(8,7);


void tal_init_transceiver(void)
{
	  radio.begin();
	  //Set up ShockBurst (disable Enhanced mode)
	  radio.setAutoAck(false);
	  radio.setRetries(0,0); //no retries

	  radio.openWritingPipe(PIPE_ADDRESS);
	  radio.openReadingPipe(1,PIPE_ADDRESS);


	  // Get into standby mode
	  radio.startListening();
	  radio.stopListening();
}


void tal_enable_transceiver(void)
{
	radio.powerUp();
}


void tal_disable_transceiver(void)
{
	radio.powerDown();
}

BOOL tal_channel_is_clear(void)
{
    return radio.testCarrier();
}


UInt8 tal_write_packet(const UInt8 * data, const UInt8 len)
{
	bool ok;

	radio.stopListening();
	ok = radio.write(data, len);
	radio.startListening();
	if( ok )
	{
		return len;
	}
	return 0;	//Write failed, no packets send.
}


BOOL tal_write_packet_done(void)
{
	return TRUE;
}


UInt8 tal_read_bytes(UInt8 * data, const UInt8 len)
{
	if( radio.read(data, len) )
	{
		return len;
	}
	return 0;
}


one_net_status_t tal_look_for_packet(tick_t duration)
{
	const uint32_t start_at = millis();
	bool available;

	do
	{
		available = radio.available();
		delay(5);	// in [ms]
	} while( (!available) && ( millis() - start_at < duration ) );

	if( available )
	{
		return ONS_SUCCESS;
	}
	return ONS_TIME_OUT;
}


one_net_status_t tal_set_data_rate(UInt8 data_rate)
{
	radio.setDataRate(RF24_1MBPS);
	return ONS_SUCCESS;
}


one_net_status_t tal_set_channel(const UInt8 channel)
{
	radio.setChannel(channel);
	return ONS_SUCCESS;
}
