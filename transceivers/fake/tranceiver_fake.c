#include "tal.h"

#if defined(ARDUINO) && ARDUINO >= 100
#include <wiring_private.h>
#else
#include <wiring.h>
#include <HardwareSerial.h>
#endif



void tal_init_transceiver(void)
{
	Serial.println(__FUNCTION__);
}


void tal_enable_transceiver(void)
{
	Serial.println(__FUNCTION__);
}


void tal_disable_transceiver(void)
{
	Serial.println(__FUNCTION__);
}

BOOL tal_channel_is_clear(void)
{
	Serial.println(__FUNCTION__);
	return TRUE;
}


UInt8 tal_write_packet(const UInt8 * data, const UInt8 len)
{
	Serial.print(__FUNCTION__);
	Serial.print(" - ");
	Serial.println(len);
	return len;
}


BOOL tal_write_packet_done(void)
{
	Serial.println(__FUNCTION__);
	return TRUE;
}


UInt8 tal_read_bytes(UInt8 * data, const UInt8 len)
{
	Serial.print(__FUNCTION__);
	Serial.print(" - ");
	Serial.println(len);
	return 0;
}


one_net_status_t tal_look_for_packet(tick_t duration)
{
	Serial.print(__FUNCTION__);
	Serial.print(" - ");
	Serial.println(duration);
	return ONS_TIME_OUT;
}


one_net_status_t tal_set_data_rate(UInt8 data_rate)
{
	Serial.print(__FUNCTION__);
	Serial.print(" - ");
	Serial.println(data_rate);
	return ONS_SUCCESS;
}


one_net_status_t tal_set_channel(const UInt8 channel)
{
	Serial.print(__FUNCTION__);
	Serial.print(" - ");
	Serial.println(channel);
	return ONS_SUCCESS;
}
