//! \defgroup CLIENT_port_specific Implementaton of ONE-NET CLIENT Port
//!   Specific Functionality
//! @{

/*
    Copyright (c) 2010, Threshold Corporation
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        * Redistributions of source code must retain the above copyright notice,
          this list of conditions, and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.
        * Neither the name of Threshold Corporation (trustee of ONE-NET) nor the
          names of its contributors may be used to endorse or promote products
          derived from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
    CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
    INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
    OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
    BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHEWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/*!
    \file client_port_specific.c
    \brief Common implementation for ONE-NET CLIENT port specific functionality.

    This file contains the common implementation (to the processor) for some of
    the CLIENT port specific ONE-NET functionality.  Interfaces come from
    client_port_specific.h and client_util.h.
*/

#include "one_net_types.h"

//#include "flash.h"
#include "one_net_port_specific.h"
#ifdef _HAS_LEDS
    #include "one_net_led.h"
#endif
#include "io_port_mapping.h"
#include "one_net.h"
#ifdef _PEER
#include "one_net_peer.h"
#endif
#include "client_util.h"


//==============================================================================
//                                  CONSTANTS
//! \defgroup CLIENT_port_specific_const
//! \ingroup CLIENT_port_specific
//! @{

//! @} CLIENT_port_specific_const
//                                  CONSTANTS END
//==============================================================================

//==============================================================================
//                                  TYPEDEFS
//! \defgroup CLIENT_port_specific_typedef
//! \ingroup CLIENT_port_specific
//! @{



//! Header to be saved when parameters are saved.
typedef struct
{
    UInt8 type;                     //!< type of data stored (see nv_data_t)
    UInt16 len;                     //!< Number of bytes that follow
} flash_hdr_t;

//! @} CLIENT_port_specific_typedef
//                                  TYPEDEFS END
//==============================================================================

//==============================================================================
//                              PRIVATE VARIABLES
//! \defgroup CLIENT_port_specific_pri_var
//! \ingroup CLIENT_port_specific
//! @{

//! Location to store next chunk of data written to flash.
//static UInt8 * nv_addr = (UInt8 *)DF_BLOCK_A_START;

//! @} CLIENT_port_specific_pri_var
//                              PRIVATE VARIABLES END
//==============================================================================

//==============================================================================
//                          PRIVATE FUNCTION DECLARATIONS
//! \defgroup CLIENT_port_specific_pri_fun
//! \ingroup CLIENT_port_specific
//! @{

//! @} CLIENT_port_specific_pri_var
//                          PRIVATE FUNCTION DECLARATIONS END
//==============================================================================

//==============================================================================
//                          PUBLIC FUNCTION IMPLEMENTATION
//! \defgroup CLIENT_port_specific_pub_func
//! \ingroup CLIENT_port_specific
//! @{

/*!
    \brief Returns a pointer to the ONE-NET CLIENT parameters.

    \param[in] type The type of memory we are looking for
    \param[out] len The number of bytes the pointer points to.

    \return 0 if there are no valid parameters or a pointer to the parameters
      if they are valid.
*/
#ifdef _PEER
const UInt8 * read_param(UInt8 type, UInt16 * const len)
#else
const UInt8 * read_param(UInt16 * const len)
#endif
{
#ifdef _DEBUGGER_USES_DATA_FLASH
    *len = 0;
    return NULL;
#else

    const UInt8 * PARAM_PTR = 0;

    if(!len)
    {
//        EXIT();
    } // if the parameter is invalid //

    *len = 0;
    if( 0 )
	{
    	const static UInt8 paramsTable[] = "ab";
		PARAM_PTR = paramsTable;
		*len = sizeof(paramsTable);
	} // if the data is valid //

    return PARAM_PTR;
#endif
} // read_param //


/*!
    \brief Clears the contents of the data flash.
    
    \param void
    
    \return void
*/
void clr_flash(void)
{
#ifdef _DEBUGGER_USES_DATA_FLASH
    return;
#endif
//    erase_data_flash(DF_BLOCK_A_START);
//    erase_data_flash(DF_BLOCK_B_START);
} // clr_flash //


/*!
    \brief Checks to see if the data flash should be erased (and erases it if
      it does).

    Checks if the uart rx & tx pins are connected to indicate that the flash
    should be erased.
    
    \param void
    
    \return void
*/
//
// dje: Eliminated superfluous nested loops.  It's valid to check
// one time.
//
void flash_erase_check(void)
{
} // flash_erase_check //


void one_net_client_save_settings(void)
{
} // one_net_client_save_settings //



//! @} CLIENT_port_specific_pub_func
//                          PUBLIC FUNCTION IMPLEMENTATION END
//==============================================================================

//==============================================================================
//                          PRIVATE FUNCTION IMPLEMENTATION
//! \defgroup CLIENT_port_specific_pri_func
//! \ingroup CLIENT_port_specific
//! @{

//! @} CLIENT_port_specific_pri_func
//                          PRIVATE FUNCTION IMPLEMENTATION END
//==============================================================================

//! @} CLIENT_port_specific
