#include "tick.h"
#if defined(ARDUINO) && ARDUINO >= 100
#include <wiring_private.h>
#else
#include <wiring.h>
#endif


void init_tick(void)
{
}


void enable_tick_timer(void)
{
}


tick_t get_tick_count(void)
{
	return millis();
}


void polled_tick_update(void)
{
}


void delay_ms(UInt16 count)
{
	delay(count);
}


void delay_100s_us(UInt16 count)
{
	delay(count/10);
	delayMicroseconds((count % 10) * 100);
}


tick_t ms_to_tick(UInt32 ms)
{
    return ms;
}
    

UInt32 tick_to_ms(tick_t num_ticks)
{
    return num_ticks;
}
