#include "one_net_port_specific.h"
#include <string.h>


void *one_net_memmove(void *dst, const void *src, size_t n)
{
	return memmove( dst, src, n );
}


void * one_net_memset (void* dst, UInt8 value, size_t len)
{
	return memset( dst, value,len );
}


void* one_net_memset_block(void* const dst, size_t size, size_t count,
  const void* const src)
{
    size_t i;
    UInt8* ptr;
    
    if (!dst) 
    { /* no recovery but at least don't crash */
        return NULL;
    }    
    
    ptr = (UInt8*) dst;
    
    for(i = 0; i < count; i++)
    {
        one_net_memmove(ptr, src, size);
        ptr += size;
    }

    return dst;
}


SInt8 one_net_memcmp(const void *vp1, const void *vp2, size_t n)
{
	return memcmp( vp1, vp2, n );
}


UInt16 one_net_byte_stream_to_int16(const UInt8 * const byte_stream)
{
	return	((((UInt16)byte_stream[0]) << 8) & 0xFF00)	|
			(((UInt16)byte_stream[1]) & 0x00FF);
}


void one_net_int16_to_byte_stream(const UInt16 val, UInt8 * const byte_stream)
{
    byte_stream[0] = (UInt8)(val >> 8);
    byte_stream[1] = (UInt8)val;
}


UInt32 one_net_byte_stream_to_int32(const UInt8 * const byte_stream)
{
	return	((((UInt32)byte_stream[0]) << 24) & 0xFF000000) |
    		((((UInt32)byte_stream[1]) << 16) & 0x00FF0000) |
    		((((UInt32)byte_stream[2]) << 8) & 0x0000FF00)	|
    		(((UInt32)byte_stream[3]) & 0x000000FF);
}


void one_net_int32_to_byte_stream(const UInt32 VAL, UInt8 * const byte_stream)
{
    byte_stream[0] = (UInt8)(VAL >> 24);
    byte_stream[1] = (UInt8)(VAL >> 16);
    byte_stream[2] = (UInt8)(VAL >> 8);
    byte_stream[3] = (UInt8)VAL;
}


UInt16 did_to_u16(const on_raw_did_t *DID)
{
    return one_net_byte_stream_to_int16(*DID) >> RAW_DID_SHIFT;
}


long int one_net_strtol(const char * str, char ** endptr, int base)
{
    // use near pointers
    //return _n_n_strtol(str, endptr, base);
	//TODO: Fix it!
	return 0;
}

