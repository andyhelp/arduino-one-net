#ifndef _UART_H
#define _UART_H

/*!
    \file uart_port_specific.h

    \brief Contains declarations for functions associated with asynchronous
      serial i/o.
*/


//==============================================================================
//						PUBLIC FUNCTION DECLARATIONS
//! \defgroup uart_pub_func
//! \ingroup uart
//! @{


/*!
    \brief Initialize the serial port.

    Port direction initialization is handled by uart_init_ports. This function
    sets the baud rate, stop bits, data bits, and parity.

    \param[in] BAUD_RATE The baud rate to set the port to.  This should be on of
      the enumerations from baud_rate_t.
    \param[in] DATA_BITS The number of data bits for each character in the
      serial transfer.  This should be one of the values from data_bits_t.
    \param[in] STOP_BITS The number of stop bits to use.  This should be one of
      the values from stop_bits_t.
    \param[in] PARITY The parity to use when transfering data.  This should be
      one of the values from parity_t.

    \return void
*/
void uart_init(const UInt8 BAUD_RATE, const UInt8 DATA_BITS,
  const UInt8 STOP_BITS, const UInt8 PARITY);


/*!
    \brief Returns the number of bytes available in the uart send buffer.

    \param void

    \return The number of bytes available in the uart send buffer.
*/
UInt16 uart_tx_bytes_free(void);


/*!
    \brief Returns the number of bytes that have been received and not yet read.

    \param void

    \return The number of bytes received, waiting to be read.
*/
UInt16 uart_rx_bytes_available(void);


/*!
    \brief Returns the size of the receive buffer.

    \param void

    \return The size of the receive buffer
*/
UInt16 uart_rx_buffer_size(void);


/*!
    \brief Returns the size of the transmit buffer.

    \param void

    \return The size of the transmit buffer
*/
UInt16 uart_tx_buffer_size(void);


/*!
    \brief Reads data that has been received over the serial port

    \param[in] data Buffer to return data in.
    \param[in] LEN The number of bytes to read (data must be at least this big)

    \return The number of bytes read
*/
UInt16 uart_read(UInt8 * const data, const UInt16 LEN);


/*!
    \brief Write data out of the serial port

    \param[in] DATA The data to be written.
    \param[in] LEN The number of bytes to be written.

    \return The number of bytes written
*/
UInt16 uart_write(const UInt8 * const DATA, const UInt16 LEN);

/*!
    \brief Write a byte in hex format out of the serial port

    \param[in] DATA The byte to be written in hex

    \return void
*/
void uart_write_int8_hex(const UInt8 DATA);


/*!
    \brief Write an array of bytes in hex format out of the serial port

    \param[in] DATA The byte to be written in hex
    \param[in] separate If TRUE, add a space between each byte
    \param[in] len The number of bytes to write

    \return void
*/
void uart_write_int8_hex_array(const UInt8* DATA, BOOL separate, UInt16 len);

//! @} uart_pub_func
//                  PUBLIC FUNCTION DECLARATIONS END
//==========================================================================

//! @} uart

#endif // #ifdef _UART_H //

